*** Settings ***
Library    SeleniumLibrary
Library    String
Library    DateTime
Library    OperatingSystem


*** Variables ***
${Browser}    Edge
${URL}        https://benytest.monilogi.sk/monilogi-angular/introduction
${viewport}=    Create Dictionary    heigtt=1920    width=1080
${Username}   test_supervisor_mf_ke@monilogi.sk
${Username1}  supervisor_mf_ke@monilogi.sk
${Password}   YLC2L4R7Rl!
${Password1}   YLC2L4R7R
${Incognito Options}  --incognito

*** Keywords ***
Valid login SuperVisor
    Open Browser    https://ca-beny-fe-test.yellowsea-f7c8ea4f.westeurope.azurecontainerapps.io/monilogi-angular/introduction    edge    --inprivate
    Maximize Browser Window
    Set Browser Implicit Wait    5s
    Wait Until Page Contains    Prihlásenie
    Click Button    Prihlásenie
    Input Text    name:loginfmt    ${Username}
    Click Button     Next
    Input Password    name:passwd   ${Password}
    Click Button     Sign in
    Click Button     Yes
    Page Should Contain Element    class:btn.btn-primary.user__logout


Menu Zalozenie klienta
    Login.Valid login SuperVisor
    #Maximize Browser Window
    Click button    Vyhľadanie/Založenie klienta
    Click button    Spravovanie klientov
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center      Spravovanie klientov
    Click Button    Založiť klienta

Generate Unique Numeric Code
    ${random_number}=    Generate Random Number
    ${unique_code}=    Convert To String    ${random_number}
    Set Test Variable    ${unique_code}

Generate Random Number
    ${random_number}=    Evaluate    random.randint(1000, 9999)
    [Return]    ${random_number}

Click On Left Menu
    ${left_menu_visible}=    Run Keyword And Return Status    Element Should Be Visible    id=nav-supervisor-order
    Run Keyword If    not ${left_menu_visible}    Expand Left Menu
    Click Element    id=panel-order-header

Expand Left Menu
    Click Element    id=panel-order-header

Visible left menu Objednavka1
    ${menu_vyhladanie_objednávok}=    Run Keyword And Return Status    Page Should Contain    Vyhľadanie objednávok
    Run Keyword If    not ${menu_vyhladanie_objednávok}    Click Button    Objednávka

Visible left menu Objednavka2
    ${menu_vyhladanie_objednávok}=    Run Keyword And Return Status    Page Should Contain    Vytvorenie objednávky
    Run Keyword If    not ${menu_vyhladanie_objednávok}    Click Button    Objednávka