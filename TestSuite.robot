*** Settings ***
Library    SeleniumLibrary
Library    String
Library    DateTime
Resource    Login.robot


*** Variables ***
${Browser}    Edge
${URL}        https://benytest.monilogi.sk/monilogi-angular/introduction
${viewport}=    Create Dictionary    heigtt=1920    width=1080
${Username}   test_supervisor_mf_ke@monilogi.sk
${Username1}  supervisor_mf_ke@monilogi.sk
${Password}   YLC2L4R7Rl!
${Password1}   YLC2L4R7R
${Incognito Options}  --incognito


*** Test Cases ***

Open Browser And Check Title
    Open Browser    https://ca-beny-fe-test.yellowsea-f7c8ea4f.westeurope.azurecontainerapps.io/monilogi-angular/introduction
    Wait Until Page Contains    Prihlásenie
    Title Should Be    Monilogi
    Close Browser


1. Valid login SuperVisor
    Open Browser    https://ca-beny-fe-test.yellowsea-f7c8ea4f.westeurope.azurecontainerapps.io/monilogi-angular/introduction    edge    --inprivate
    Set Browser Implicit Wait    5s
    Click Button    Prihlásenie
    Input Text    name:loginfmt    ${Username}
    Click Button     Next
    Input Password    name:passwd   ${Password}
    Click Button     Sign in
    Click Button     Yes
    Page Should Contain Element    class:btn.btn-primary.user__logout
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center    Supervisor
    Close Browser


2. Invalid login SuperVisor / Incorrect email
    Open Browser    https://ca-beny-fe-test.yellowsea-f7c8ea4f.westeurope.azurecontainerapps.io/monilogi-angular/introduction    edge    --inprivate
    Set Browser Implicit Wait    5s
    Click Button    Prihlásenie
    Input Text    name:loginfmt    ${Username1}
    Click Button     Next
    Wait Until Element Is Visible    class:col-md-24.error.ext-error
    Close Browser

3. Invalid login SuperVisor / Incorrect password
    Open Browser    https://ca-beny-fe-test.yellowsea-f7c8ea4f.westeurope.azurecontainerapps.io/monilogi-angular/introduction    edge    --inprivate
    Set Browser Implicit Wait    5s
    Click Button    Prihlásenie
    Input Text    name:loginfmt    ${Username}
    Click Button     Next
    Input Password    name:passwd   ${Password1}
    Click Button     Sign in
    Wait Until Element Is Visible    id:passwordError
    Close Browser
    
4. Logout SuperVisor
    Open Browser    https://ca-beny-fe-test.yellowsea-f7c8ea4f.westeurope.azurecontainerapps.io/monilogi-angular/introduction    edge    --inprivate
    Set Browser Implicit Wait    5s
    Click Button    Prihlásenie
    Input Text    name:loginfmt    ${Username}
    Click Button     Next
    Input Password    name:passwd   ${Password}
    Click Button     Sign in
    Click Button     Yes
    Page Should Contain Element    class:btn.btn-primary.user__logout
    Click button    class:btn.btn-primary.user__logout
    Wait Until Element Is Visible    class:btn.btn-primary.btn-large
    # Set Selenium Timeout    15 seconds
    Close Browser


5. SuperVisor / Bocne menu / Objednavka / Vyhľadanie objednávok
    Login.Valid login SuperVisor
    Login.Click On Left menu
    Login.Expand Left Menu
    Login.Visible left menu Objednavka1
    Click button      Vyhľadanie objednávok
    Click Element    class:icon
    Element Should Contain    css=.center    Vyhľadanie objednávok
    Click Element        xpath=//li[contains(text(), 'SLSP')]
    Click Button    Potvrdiť
    Page Should Contain Element    css=.table-heading-item    Úloha
    Page Should Contain Element    css=.table-heading-item    Stav úlohy
    Page Should Contain Element    css=.table-heading-item    ID dávky
    Page Should Contain Element    css=.table-heading-item    ID obj.
    Page Should Contain Element    css=.table-heading-item    Typ obj.
    Page Should Contain Element    css=.table-heading-item    Stav obj.
    Page Should Contain Element    css=.table-heading-item     Prepravca
    Page Should Contain Element    css=.table-heading-item     Pobočka / Klient
    Page Should Contain Element    css=.table-heading-item     Mena
    Page Should Contain Element    css=.table-heading-item     Objem
    Page Should Contain Element    css=.table-heading-item     Dátum vytvorenia
    Page Should Contain Element    css=.table-heading-item      Dátum ukončenia
    Element Should Not Contain    css=.table-heading-item    Stredisko ML
    Double Click Element    class:row__container
    Element Should Contain    css=.center     Objednávka
    Close Browser


6. SuperVisor / Bocne menu / Objednavka / Vytvorenie objednavky
    Login.Valid login SuperVisor
    Login.Click On Left menu
    Login.Expand Left Menu
    Login.Visible left menu Objednavka2
    Click button    Vytvorenie objednávky
    Wait Until Page Contains Element    css=.center    30s
    Wait Until Element Contains    css=.center    Vytvorenie objednávky
    Close Browser
    
7. SuperVisor / Bocne menu / Plánovanie / Odvody
    Login.Valid login SuperVisor
    Login.Click On Left menu
    Login.Expand Left Menu
    Click button    Plánovanie
    Click button    Odvody
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center     Plánovanie objednávok - odvod do ML
    Close Browser

8. SuperVisor / Bocne menu / Plánovanie / Dotácie
    Login.Valid login SuperVisor
    Login.Click On Left menu
    Login.Expand Left Menu
    Click button    Plánovanie
    Click button    Dotácie
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center     Plánovanie objednávok - dotácie z ML
    Close Browser

9. SuperVisor / Bocne menu / Plánovanie / Import
    Login.Valid login SuperVisor
    Login.Click On Left menu
    Login.Expand Left Menu
    Click button    Plánovanie
    Click button    Import objednávok
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center     Import objednávok
    Close Browser


10. SuperVisor / Bocne menu / Vyhľadanie klienta
    Login.Valid login SuperVisor
    Login.Click On Left menu
    Login.Expand Left Menu
    Click button    Vyhľadanie/Založenie klienta
    Click button    Spravovanie klientov
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center      Spravovanie klientov
    #Click Element    class=icon
    Click Element    css=.select-label.label
    Wait Until Element Is Visible    css=.menu.list-group[data-cy='select-menu']
    Click Element    xpath=//li[@data-cy='select-option' and contains(text(),'Tatrabanka')]
    Click Button    Potvrdiť
    Double Click Element    class=hoverable
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center    Detail klienta
    Close Browser


11. SuperVisor / Bocne menu / Fit/Ruličkovanie
   Login.Valid login SuperVisor
   Login.Click On Left menu
   Login.Expand Left Menu
   Click button    Fit/Ruličkovanie
   Click Element    id:nav-supervisor-fitRollOverview
   Wait Until Page Contains Element    css=.center    15s
   Wait Until Element Contains    css=.center    Fit/Ruličkovanie
   Close Browser

12. SuperVisor / Bocne menu / Založenie klienta / Error message
    Login.Valid login SuperVisor
    Login.Click On Left menu
    Login.Expand Left Menu
    Click button    Vyhľadanie/Založenie klienta
    Click button    Spravovanie klientov
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center      Spravovanie klientov
    Click Button    Založiť klienta
    Click Element    class=icon
    Wait Until Element Is Visible    css=.menu.list-group[data-cy='select-menu']
    Click Element    xpath=//li[@data-cy='select-option' and contains(text(),'Tatrabanka')]
    Scroll Element Into View    id=btn-client-confirm
    Click Button    Uložiť
    Wait Until Page Contains    Nie sú vyplnené povinné polia. Záznam nie je možné uložiť.
    Close Browser
    
13. SuperVisor / Bocne menu / Založenie duplicitného klienta
    Login.Menu Zalozenie klienta
    Login.Click On Left menu
    Login.Expand Left Menu
    Click Element    class=icon
    Wait Until Element Is Visible    css=.menu.list-group[data-cy='select-menu']
    Click Element    xpath=//li[@data-cy='select-option' and contains(text(),'Tatrabanka')]
    Wait Until Element Is Visible    css=.input-group [data-cy=input-input-original]    timeout=10s
    Input Text    css=.input-group [data-cy=input-input-original]    76567890
    Input Text    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[3]/div[1]/app-input/div/div/input[1]    Drevostavka sro
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[4]/div[1]/app-select/div/div/div/span[2]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[4]/div[1]/app-select/div/div/div/span[2]
    Wait Until Element Is Visible    xpath=//li[contains(text(), 'ŤAŽBA A DOBÝVANIE')]
    Click Element    xpath=//li[contains(text(), 'ŤAŽBA A DOBÝVANIE')]
    Input Text    xpath:/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[5]/div[1]/app-input/div/div/input    76567890
    Input Text    xpath:/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[2]/div[2]/app-input/div/div/input    Ulica
    Input Text    xpath:/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[3]/div[2]/app-input/div/div/input[1]    Mesto
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Input Text    id=input-zip    098 76
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[3]/app-select/div/div/div/span[2]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[3]/app-select/div/div/div/span[2]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[3]/app-select/div/div/ul/li[contains(text(), 'NIVY_Name')]
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[1]/app-checkbox/div/div/input    timeout=15s
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[1]/app-checkbox/div/div/input
    ${current_date}=    Get Current Date
    ${formatted_date}=    Convert Date    ${current_date}    result_format=%d.%m.%Y
    Input Text    id=datepicker-valid-from    ${formatted_date}
    Input Text    id=datepicker-valid-to    ${formatted_date}
    # skryty element s id=input-limit
    Execute JavaScript    document.getElementById('input-limit').style.display = 'block';
    Wait Until Element Is Visible    id=input-limit
    Input Text    id=input-limit    5500
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[6]/div[1]/app-select/div/div
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[6]/div[1]/app-select/div/div
    Click Element    xpath=//li[@data-cy='select-option' and contains(text(), 'Klient')]
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[4]/div[2]/div[1]/div[1]/app-select/div/div/div/span[2]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[4]/div[2]/div[1]/div[1]/app-select/div/div/div/span[2]
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[4]/div[2]/div[1]/div[1]/app-select/div/div/ul/li[1]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[4]/div[2]/div[1]/div[1]/app-select/div/div/ul/li[1]
    Input Text    id=input-aml-desc    AUTOMAT
    Scroll Element Into View    id=btn-client-confirm    
    Click Button    Uložiť
    Wait Until Page Contains    Klient už existuje
    Close Browser

    
13. SuperVisor / Bocne menu / Založenie nového klienta
    Login.Menu Zalozenie klienta
    Login.Click On Left menu
    Login.Expand Left Menu
    Click Element    class=icon
    Wait Until Element Is Visible    css=.menu.list-group[data-cy='select-menu']
    Click Element    xpath=//li[@data-cy='select-option' and contains(text(),'Tatrabanka')]
    Wait Until Element Is Visible    css=.input-group [data-cy=input-input-original]    timeout=10s
    ${unique_code}=    Generate Random String    10    [NUMBERS]
    Input Text    css=.input-group [data-cy=input-input-original]    ${unique_code}
    #Input Text    css=.input-group [data-cy=input-input-original]    76567890
     ${unique_text}=    Generate Random String    10    [ASCII]
    Input Text    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[3]/div[1]/app-input/div/div/input[1]    ${unique_text}
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[4]/div[1]/app-select/div/div/div/span[2]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[4]/div[1]/app-select/div/div/div/span[2]
    Wait Until Element Is Visible    xpath=//li[contains(text(), 'ŤAŽBA A DOBÝVANIE')]
    Click Element    xpath=//li[contains(text(), 'ŤAŽBA A DOBÝVANIE')]
    Input Text    xpath:/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[5]/div[1]/app-input/div/div/input    76567890
    Input Text    xpath:/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[2]/div[2]/app-input/div/div/input    Ulica
    Input Text    xpath:/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[3]/div[2]/app-input/div/div/input[1]    Mesto
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Input Text    id=input-zip    098 76
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[3]/app-select/div/div/div/span[2]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[3]/app-select/div/div/div/span[2]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[3]/app-select/div/div/ul/li[contains(text(), 'NIVY_Name')]
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[1]/app-checkbox/div/div/input    timeout=15s
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[2]/div[2]/div[2]/div[1]/app-checkbox/div/div/input
    ${current_date}=    Get Current Date
    ${formatted_date}=    Convert Date    ${current_date}    result_format=%d.%m.%Y
    Input Text    id=datepicker-valid-from    ${formatted_date}
    Input Text    id=datepicker-valid-to    ${formatted_date}
    # skryty element s id=input-limit
    Execute JavaScript    document.getElementById('input-limit').style.display = 'block';
    Wait Until Element Is Visible    id=input-limit
    Input Text    id=input-limit    5500
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[6]/div[1]/app-select/div/div
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[1]/div[2]/div[6]/div[1]/app-select/div/div
    Click Element    xpath=//li[@data-cy='select-option' and contains(text(), 'Klient')]
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[4]/div[2]/div[1]/div[1]/app-select/div/div/div/span[2]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[4]/div[2]/div[1]/div[1]/app-select/div/div/div/span[2]
    Wait Until Element Is Visible    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[4]/div[2]/div[1]/div[1]/app-select/div/div/ul/li[1]
    Click Element    xpath=/html/body/app-root/app-layout/div/div[1]/div[2]/app-client/div/form/div[4]/div[2]/div[1]/div[1]/app-select/div/div/ul/li[1]
    Input Text    id=input-aml-desc    AUTOMAT
    Scroll Element Into View    id=btn-client-confirm    
    Click Button    Uložiť
    Wait Until Page Contains Element    css=.center    15s
    Wait Until Element Contains    css=.center      Spravovanie klientov
    Close Browser
















    


